
#include "si_toolchain.h"
#include "C8051F330_defs.h"
#include <stdio.h>
#include "timer.h"


//-----------------------------------------------------------------------------
// 16-bit SFR Definitions for 'F33x
//-----------------------------------------------------------------------------

static int _overflow_count = 0;

//-----------------------------------------------------------------------------
// Timer2_ISR
//-----------------------------------------------------------------------------
//
// This routine implemens potentiometer control, synchronous with Timer2 interrupts
//
void Timer2_ISR (void) interrupt 5
{
   TF2H = 0;                              // clear Timer2 interrupt flag
   _overflow_count++;
}

int Timer2_IsTimeout(void)
{
	if( _overflow_count > 0){
		_overflow_count = 0;
		return 1;
	}
	
	return 0;
}

void Timer2_Init (int counts)
{
   TMR2CN  = 0x00;                        // stop Timer2; clear TF2; use Timer2 as 16-bit timer with SYSCLK/12 as timebase
   CKCON  &= ~0x60;                       // Timer2 clocked based on T2XCLK;

   TMR2RL  = -counts;                     // Init reload values
   TMR2    = 0xffff;                      // set to reload immediately
   ET2     = 1;                           // enable Timer2 interrupts
   TR2     = 1;                           // start Timer2
}