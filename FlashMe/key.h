#ifndef _KEY_H_
#define _KEY_H_

typedef enum _enKey
{
    KEY_POWER = 0,
    KEY_MAX_NUM
} enKey;
 
typedef enum _enKeyState{
    KEY_NULL= 0,
    KEY_UP,
    KEY_DOWN,
    KEY_LONG
} enKeyState;

typedef struct _stKey{
    enKey c;
    enKeyState state;
    int count;
}stKey;


enKeyState Powerkey_Scan(void);
void Powerkey_init(void);

#endif