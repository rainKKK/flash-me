#ifndef _TIMER_H_
#define _TIMER_H_

void Timer2_Init(int counts);
int Timer2_IsTimeout(void);

#endif