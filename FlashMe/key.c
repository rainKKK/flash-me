#include "si_toolchain.h"
#include "C8051F330_defs.h"
#include <stdio.h>
#include "key.h" 

sbit POWER_KEY = P0^2;

#define KEY_DEBOUNCE_PERIOD     2   // 延时消抖时间
#define KEY_LONG_PERIOD         100

static stKey _power_key = { KEY_POWER, KEY_NULL, 0 };

void Powerkey_init(void)
{
	POWER_KEY = 0;
}

enKeyState Powerkey_Scan(void)
{
    if(POWER_KEY == 1){
        _power_key.count++;
        if(_power_key.count >= KEY_LONG_PERIOD){
           _power_key.state = KEY_LONG;
        }else if(_power_key.count >= KEY_DEBOUNCE_PERIOD){
           _power_key.state = KEY_DOWN;
				}
    }else {
        if(_power_key.state > KEY_UP){
            _power_key.state  = KEY_UP;
            _power_key.count = 0;
        }
    }

    return _power_key.state;
}