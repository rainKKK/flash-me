/*
 * Copyright (c) 2020, yifengling0. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. No personal names or organizations' names associated with the
 *    Atomthreads project may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ATOMTHREADS PROJECT AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include "console.h"
#include <string.h>
#include "sio.h"


#define MAX_PARAM_COUNT (5)

SLIST_HEAD(Pool, Node);
static struct Pool xdata _header;

static char* xdata _parameters[MAX_PARAM_COUNT] = { NULL };



static enConsoleResult none(int argc, char** argv)
{
	return CE_OK;
}


static const COMMAND _noneCommand = {
	DEFAULT_NODE,
	"",
	none,
};

/* Application threads' TCBs */
#define CMD_LEN (32)
typedef struct _tCommand {
	char buffer[CMD_LEN];
	int index;
}tCommand;

static tCommand xdata _command;

void ConsoleInit()
{
	_command.index = 0;
	memset(_command.buffer, 0, CMD_LEN);

	SLIST_INIT(&_header);
	SLIST_INSERT_HEAD(&_header, (Node*)&_noneCommand, node);
}

void ConsoleRun()
{
	enConsoleResult result;

	char c = com_getchar();
	if( c != -1 ){
		if (c == 0x0d) {
			printf("\r\n");
			result = ConsoleInput(_command.buffer);
			memset(_command.buffer, 0, CMD_LEN);
			_command.index = 0;
		}
		else {
			_command.buffer[_command.index] = c;
			_command.index++;
		}
	}
}

void ConsoleRegCmd(PCOMMAND cmd)
{
	SLIST_INSERT_HEAD(&_header, (Node*)cmd, node);
}

static enConsoleResult RunCommand(int argc, char** argv)
{
	enConsoleResult ret = CE_NOT_FOUND;
	Node* item;

	SLIST_FOREACH(item, &_header, node) {
		COMMAND* cmd = (COMMAND*)item;
		if (strcmp(cmd->name, argv[0]) == 0) {
			ret = cmd->action(argc, argv);
			break;
		}
	}

	return ret;
}

enConsoleResult ConsoleInput(char *command)
{
	enConsoleResult ret;
	int count = 0;
	char* current = command;
	int remind = 0;
	char *p;

	for(p = command; *p != '\0'; p++){
		if (' ' == *p) {
			_parameters[count] = current;
			*p = '\0';
			current = p + 1;
			count++;
			remind = 0;
		}
		else {
			remind = 1;
		}
	}

	if (count == 0 || remind > 0) {
		_parameters[count] = current;
		count++;
	}
	
	ret = RunCommand(count, _parameters);

	return ret;
}
