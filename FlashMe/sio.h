#ifndef _SIO_H_
#define _SIO_H_


void com_initialize (void);

void com_baudrate (
  unsigned long baudrate);

char com_putchar (
  unsigned char c);

int com_getchar (void);

unsigned char com_rbuflen (void);

unsigned char com_tbuflen (void);

int com_write(unsigned char* buf, int size);

#endif








